package org.elena.autowiring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class Farm implements ApplicationContextAware, BeanNameAware, InitializingBean, DisposableBean {

//    private Animal cow;
//    private Animal dog;
//    private Animal pig;

    private ApplicationContext context = null;

//    public void defineAnimal() {
//        System.out.println(cow);
//        System.out.println(dog);
//        System.out.println(pig);
//    }

//    public Animal getCow() {
//        return cow;
//    }
//
//    public void setCow(Animal cow) {
//        this.cow = cow;
//    }
//
//    public Animal getDog() {
//        return dog;
//    }
//
//    public void setDog(Animal dog) {
//        this.dog = dog;
//    }
//
//    public Animal getPig() {
//        return pig;
//    }
//
//    public void setPig(Animal pig) {
//        this.pig = pig;
//    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public void setBeanName(String beanName) {
        System.out.println("Bean name is : " + beanName);
    }


    private List<Animal> animals;

    public void defineAnimal() {
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    // This is the method to be called after the initialiation.
    public void afterPropertiesSet() throws Exception {
        System.out.println("Initializing beans init method code.");
    }

    public void destroy() throws Exception {
        System.out.println("I am getting destroyed.(*-|*-)");
    }


    public void myInit() {
        System.out.println("Initializing beans with my own method without implementing any interface");
    }
}
