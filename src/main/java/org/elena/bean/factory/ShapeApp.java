package org.elena.bean.factory;

import com.sun.xml.internal.ws.model.AbstractWrapperBeanGenerator;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */
public class ShapeApp {

    public static void main(String[] args) {
        // Triangle triangle = new Triangle();
        // 1.Configures the bean factory using the xml file containing specifications about the beans.
        // BeanFactory beanFactory = new XmlBeanFactory(new FileSystemResource("/home/elena/Coding/spring-learning/src/main/resources/spring.xml"));
        // Triangle triangle = (Triangle) beanFactory.getBean("triangle");

        // Second method of configuring the bean factory. This time the xml does not need to be a FileSystemResource.
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/home/elena/Coding/spring-learning/src/main/resources/spring.xml");
//        Triangle triangle = (Triangle) applicationContext.getBean("triangle");
//        triangle.draw();
    }
}
