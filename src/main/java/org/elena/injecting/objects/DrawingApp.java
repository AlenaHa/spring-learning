package org.elena.injecting.objects;

import org.elena.bean.factory.*;
import org.elena.bean.factory.Triangle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class DrawingApp {

    public static void main(String[] args) {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");

        // The bean must be casted into the object class we ask for.
        //Point zeroPoint = (Point) applicationContext.getBean("zeroPoint");
//        Triangle2 triangle2 = (Triangle2) applicationContext.getBean("triangle2");
//        triangle2.draw();
//
//        Circle circle = (Circle) applicationContext.getBean("circle");
//        circle.draw();


        // Instead of creating multiple objects, it's enougn using the e=interface to call the methods for the objects that
        // implement the interface.
        Shape shape = (Shape) applicationContext.getBean("triangle2");
        shape.draw();
    }

}
