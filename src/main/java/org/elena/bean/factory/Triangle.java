package org.elena.bean.factory;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */
public class Triangle {
    private String type;


    // Constructor injection -> need to change things in xml config file
    public Triangle(String type) {
        this.type = type;
    }

    public void draw() {
        System.out.println("Triangle drown");
    }

    public String getType() {
        return type;
    }

    // Setter injection
    public void setType(String type) {
        this.type = type;
    }
}
