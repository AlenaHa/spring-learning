package org.elena.autowiring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class FarmApp {

    public static void main(String[] args) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        // When the app ends, it knows that it should destroy all the beans.
        context.registerShutdownHook();
        Farm farm = (Farm) context.getBean("childFarm3");
        farm.defineAnimal();
    }
}
