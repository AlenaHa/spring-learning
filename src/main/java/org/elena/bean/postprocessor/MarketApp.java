package org.elena.bean.postprocessor;

import org.elena.dependency.injection.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Elena Hardon
 * @date 3/17/17.
 */
public class MarketApp {
    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        context.registerShutdownHook();
        Market market = (Market) context.getBean("market");

        // Because all of the beans defined previously in the xml have defines the basic scope singleton, the  post proc. methods
        // will be called for each bean initialization. Therefore, the methods are called a lot of times.
        market.iDoNothing();

        // Getting the message from the config.properties file with id:greeting and value :Hello!
        System.out.println(context.getMessage("greeting", null, "If i don't find the message. Default greeting", null));
    }
}
