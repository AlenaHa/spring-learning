package org.elena.autowiring;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class Animal {
    private String name;
    private String colour;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Animal{");
        sb.append("name='").append(name).append('\'');
        sb.append(", colour='").append(colour).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
