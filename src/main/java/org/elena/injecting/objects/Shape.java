package org.elena.injecting.objects;

/**
 * @author Elena Hardon
 * @date 3/20/17.
 */
public interface Shape {
    void draw();
}
