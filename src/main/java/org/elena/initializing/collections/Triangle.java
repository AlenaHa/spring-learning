package org.elena.initializing.collections;

import java.util.List;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class Triangle {

    List<Point> points;

    void draw() {

        for (Point point : points) {
            System.out.println(point);
        }
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
