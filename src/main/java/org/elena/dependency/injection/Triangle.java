package org.elena.dependency.injection;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */
public class Triangle implements Shape {

    public String draw() {
        return "Drawing a triangle.";
    }
}
