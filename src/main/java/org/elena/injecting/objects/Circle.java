package org.elena.injecting.objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

/**
 * @author Elena Hardon
 * @date 3/20/17.
 */
public class Circle implements Shape {

    private Point center;

    public void draw() {
        System.out.println("Circle point is: " + center);
    }

    public Point getCenter() {
        return center;
    }

    // This annotation tells spring that this is a required member variable. If this is not satisfied spring throws an exception.
    //@Required
    @Autowired
    @Qualifier("circleRelated")
    public void setCenter(Point center) {
        this.center = center;
    }
}
