package org.elena.bean.postprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;


/**
 * @author Elena Hardon
 * @date 3/17/17.
 */

@Component
@Service
public class Market implements IVeggie {
    private Vegetable carrot;
    private Vegetable potato;
    private Vegetable onion;
    private MessageSource messageSource;

    public Vegetable getCarrot() {
        return carrot;
    }

    @Resource(name = "Carrot")
    public void setCarrot(Vegetable carrot) {
        this.carrot = carrot;
    }

    public Vegetable getPotato() {
        return potato;
    }

    @Resource(name = "Potato")
    public void setPotato(Vegetable potato) {
        this.potato = potato;
    }

    public Vegetable getOnion() {
        return onion;
    }

    @Resource(name = "Onion")
    public void setOnion(Vegetable onion) {
        this.onion = onion;
    }

    // The method will be called when the market is initialized.
    @PostConstruct
    public void initializeMarket() {
        System.out.println("Init of the market");
    }

    // The method will be called before the destruction of the beans.
    @PreDestroy
    public void destroyMarket() {
        System.out.println("Destroy the market.");
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    // Autowired by type.
    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public void iDoNothing() {
        System.out.println(onion);
        System.out.println(carrot);
        System.out.println(potato);
        System.out.println(this.getMessageSource().getMessage("greeting", null, "I am called from the market class", null));
    }
}
