package org.elena.initializing.collections;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class DrawingApp {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");

        // The bean must be casted into the object class we ask for.
        //Point zeroPoint = (Point) applicationContext.getBean("zeroPoint");
        Triangle triangle = (Triangle) applicationContext.getBean("triangle3");
        triangle.draw();
    }
}
