package org.elena.bean.postprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author Elena Hardon
 * @date 3/17/17.
 */
public class DisplayBeanNamePostProcessor implements BeanPostProcessor {

    /**
     * @param bean     The object for which the bean postProcessInit is proceeded
     * @param beanName The bean name/id for the object.
     * @return The object expected from the named bean. Springs waits for you to return the object back so it can continue the configuration.
     * @throws BeansException
     */
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("Before initialization method.Bean name is: " + beanName);
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("After the initialization method.Bean name is: " + beanName);
        return bean;
    }
}
