package org.elena.dependency.injection;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */
public interface Shape {
    public String draw();
}
