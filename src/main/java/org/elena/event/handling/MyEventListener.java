package org.elena.event.handling;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.EventListener;

/**
 * @author Elena Hardon
 * @date 3/20/17.
 */

@Component
public class MyEventListener implements ApplicationListener {

    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        System.out.println(applicationEvent.toString());
    }
}
