package org.elena.dependency.injection;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */

// This class assumes that someone is instantiating the shape(circle or triangle)
// This class also doesn't know what is depending on -> this way the dependencies are removed.
// The dependency is injected by the entity "shape".
public class Drawing {
    private Shape shape;

    public void drawShape() {
        this.shape.draw();
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }
}
