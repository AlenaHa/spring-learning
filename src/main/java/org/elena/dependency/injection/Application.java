package org.elena.dependency.injection;

/**
 * @author Elena Hardon
 * @date 3/12/17.
 */
public class Application {

    Triangle triangle;
    Circle circle;

    Application() {
        this.triangle = new Triangle();
        this.circle = new Circle();
    }

    /**
     * This method will draw the shape.
     *
     * @param shape It could be either a triangle or a circle.
     */
    public void myDrawMethod(Shape shape) {
        // if the shape is a triangle, the method it will draw a triangle.
        // somewhere in the class , in order to call this method, the object(triangle or circle) must be instantiated.
        shape.draw();
    }
    /*
     * Old way of calling the methods, without the shape interface.
     public static void main(String[] args) {
     Triangle triangle = new Triangle();
     triangle.draw();
     Circle circle = new Circle();
     circle.draw();
     }
     */
}
