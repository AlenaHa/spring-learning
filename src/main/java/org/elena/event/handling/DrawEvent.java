package org.elena.event.handling;

import org.springframework.context.ApplicationEvent;

/**
 * @author Elena Hardon
 * @date 3/20/17.
 */
public class DrawEvent extends ApplicationEvent {

    public DrawEvent(Object source) {
        super(source);
    }

    // used in injection objects package.
    public String toString() {
        return "Event draw recorded.";
    }
}
