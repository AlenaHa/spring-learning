package org.elena.injecting.objects;

import org.elena.event.handling.DrawEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 * @author Elena Hardon
 * @date 3/16/17.
 */
public class Triangle2 implements Shape, ApplicationEventPublisherAware {

    private Point pointA;
    private Point pointB;
    private Point pointC;

    private ApplicationEventPublisher eventPublisher;

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public void draw() {
        System.out.println("Point A " + getPointA());
        System.out.println("Point B " + getPointB());
        System.out.println("Point C " + getPointC());
        eventPublisher.publishEvent(new DrawEvent(this.getClass()));
    }

    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
}
